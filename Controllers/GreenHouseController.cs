﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using biovegwww.Models;

namespace biovegwww.Controllers
{
    /// <summary>
    /// Handles CRUD's of <see cref="GreenHouse"/>
    /// </summary>
    public class GreenHouseController
    {
        public List<GreenHouse> GreenHouses { get; }

        /// <summary>
        /// Creates a new <see cref="GreenHouse"/>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        public void CreateGreenHouse(int id, string name, string email)
        {
            // Need db call
            GreenHouses.Add(new GreenHouse(id, name, email));
        }

        /// <summary>
        /// Returns a specific <see cref="GreenHouse"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GreenHouse ReadGreenHouse(int id)
        {
            // Need db call
            return null;
        }

        /// <summary>
        /// Updates a specific <see cref="GreenHouse"/>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public GreenHouse UpdateGreenHouse(int id, string name, string email)
        {
            // Need db call
            return null;
        }

        /// <summary>
        /// Deletes a specific <see cref="GreenHouse"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GreenHouse DeleteGreenHouse(int id)
        {
            // Need db call
            return null;
        }

        /// <summary>
        /// Returns a list of all <see cref="GreenHouse"/> related to the user
        /// </summary>
        /// <returns></returns>
        public List<GreenHouse> GetAllGreenHouses()
        {
            // Need db call
            return GreenHouses;
        }
    }
}