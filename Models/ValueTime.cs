using System;

namespace biovegwww.Models
{
    public class ValueTime
    {
        public float Value { get; set; }
        public DateTime DateTime { get; set; }
        
    }
}