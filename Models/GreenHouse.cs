﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biovegwww.Models
{
    public class GreenHouse
    {
        public int Id { get; }
        public string Name { get; }
        public string Email { get; }

        public GreenHouse(int id, string name, string email)
        {
            Id = id;
            Name = name;
            Email = email;
        }
    }
}
