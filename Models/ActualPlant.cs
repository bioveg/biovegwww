﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biovegwww.Models
{
    public class ActualPlant
    {
        public string Id { get; set; }
        public string Plant { get; set; }

        public ActualPlant(string plantName, string sensorId)
        {
            Id = sensorId;
            Plant = plantName;
        }
    }
}
