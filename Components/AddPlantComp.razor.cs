﻿using biovegwww.Models;
using biovegwww.Repositories;
using Microsoft.AspNetCore.Components;

namespace biovegwww.Components
{
    public partial class AddPlantComp
    {
        [Parameter]
        public EventCallback OnPlantChanged { get; set; }

        [Parameter]
        public int GreenHouseId { get; set; }

        public string SensorId { get; set; }
        public string PlantName { get; set; } = "";

        public PlantRepository PlantRepository { get; set; } = new PlantRepository();

        public ActualPlant Plant { get; set; } = new ActualPlant("", "");

        bool showAddPlantComp = false;

        void AddPlantCompShow() => showAddPlantComp = true;
        void AddPlantCompCancel() => showAddPlantComp = false;
        async void AddPlantCompOk()
        {
            showAddPlantComp = false;

            Plant = await PlantRepository.CreatePlant(GreenHouseId, PlantName, SensorId);

            await OnPlantChanged.InvokeAsync();

            PlantName = "";
        }
    }
}
