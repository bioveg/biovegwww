﻿using biovegwww.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace biovegwww.Components
{
    public partial class SensorLineGraph
    {
        [Parameter] public string ValueType { get; set; }
        [Parameter] public string Id { get; set; }

        private HubConnection _hubConnection;
        private readonly bool smooth = true;
        private List<ValueTime> values = new();
        private int timeBetweenUpdatesInSeconds = 3600;

        protected async override void OnInitialized()
        {
            await StartHubConnection();
            AddChartDataListener();
            GetSensorData();
        }

        private async Task StartHubConnection()
        {
            _hubConnection = new HubConnectionBuilder().WithUrl("http://wgniash.duckdns.org:15000/webservicehub").Build();
            await _hubConnection.StartAsync();

        }


        private void AddChartDataListener()
        {
            //_hubConnection.On<List<ValueTime>>("ReceiveSensorData", (data) =>
            _hubConnection.On<List<ValueTime>>("ReceiveSensorData", (data) =>
            {
                values = data;
                Console.WriteLine($"data points = {data.Count}");
                Console.WriteLine($"value of first datapoint {data[0].Value}");
                StateHasChanged();
            });

        }

        private void GetSensorData()
        {
            _hubConnection.InvokeAsync("GetSensorData", Id, 2, timeBetweenUpdatesInSeconds);
        }

        public void Dispose()
        {
            _hubConnection.DisposeAsync();
        }
    }
}
