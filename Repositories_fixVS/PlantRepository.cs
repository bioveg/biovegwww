﻿using biovegwww.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace biovegwww.Repositories
{
    public class PlantRepository
    {
        private HttpClient client;
        private string encodedData;
        private List<ActualPlant> plants;
        private byte[] data;
        private string urlStart = "http://wgniash.duckdns.org:15000/Sensor?";
        private ActualPlant Plant;

        public PlantRepository()
        {
            client = new HttpClient();
            Plant = new ActualPlant("", "");
            plants = new List<ActualPlant>();
        }

        /// <summary>
        /// Uses email to request a List of <see cref="Plant"/> associated with the email.
        /// </summary>
        /// <param name="email"></param>
        public async Task<List<ActualPlant>> GetAllPlants(int greenHouseId)
        {

            HttpResponseMessage response = await client.GetAsync(urlStart + "greenhouseid=" + greenHouseId);

            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsByteArrayAsync();

                encodedData = Encoding.UTF8.GetString(data);
                Console.WriteLine(encodedData);
                plants = JsonConvert.DeserializeObject<List<ActualPlant>>(encodedData);
            }
            return plants;
        }

        /// <summary>
        /// Posts a new <see cref="Plant"/> to the API.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        public async Task<ActualPlant> CreatePlant(int greenhouseId, string plantName, string sensorId)
        {
            var response = await client.PostAsync(urlStart + "id=" + sensorId + "&greenhouseid=" + greenhouseId + "&plant=" + plantName, null);

            string responseString = await response.Content.ReadAsStringAsync();

            ActualPlant plantToReturn = JsonConvert.DeserializeObject<ActualPlant>(responseString);

            return plantToReturn;
        }
    }
}
