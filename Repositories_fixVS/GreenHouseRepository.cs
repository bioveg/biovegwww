﻿using biovegwww.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace biovegwww.Repositories
{
    public class GreenHouseRepository
    {
        private HttpClient client;
        public GreenHouseRepository()
        {
            client = new();
            client.BaseAddress = new Uri("http://wgniash.duckdns.org:15000/");
            Console.WriteLine(client.BaseAddress.Scheme);
        }
        /// <summary>
        /// Uses email to request a List of <see cref="GreenHouse"/> associated with the email.
        /// </summary>
        /// <param name="email"></param>
        public async Task<List<GreenHouse>> GetAllGreenHouses(string email)
        {
            List<GreenHouse> greenHouses = new();
            HttpResponseMessage response = await client.GetAsync("GreenHouse?email=" + email);

            if (response.IsSuccessStatusCode)
            {
                byte[] data = await response.Content.ReadAsByteArrayAsync();

                string encodedData = Encoding.UTF8.GetString(data);

                greenHouses = JsonConvert.DeserializeObject<List<GreenHouse>>(encodedData);
            }
            return greenHouses;
        }

        /// <summary>
        /// Posts a new <see cref="GreenHouse"/> to the API.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        public async Task<GreenHouse> CreateGreenHouse(string name, string email)
        {
            var response = await client.PostAsync("GreenHouse?name=" + name + "&email=" + email, null);
            string responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<GreenHouse>(responseString);
        }
    }
}
